class Stack {
    constructor(n) {
        this.items = new Array(n);
        console.log('item length:', this.items.length);
        this.count = 0;
        this.n = n;
    }
    push(item) {
        if (this.count === this.n) return false;
        this.items[this.count] = item;
        this.count++;
    }
    pop() {
        if (this.count === 0) return false;
        return this.items[--this.count];
    }
    display() {
        if (this.count === 0) return false;
        for(let i = 0; i < this.count; i++) {
            process.stdout.write(this.items[i]);
        }
        console.log('\n------------------------')
    }
}

const stack = new Stack(3);
stack.push('g');
stack.push('g1');
stack.push('g3');
stack.push('g4');
stack.display();
stack.pop();
stack.display();


module.exports = {};