/**
 * 1）无处不在的二分思想
 */
module.exports = {}

/**
 * 二分查找
 * @param key
 * @param arr
 */
function bsearch(arr, key) {
    let low = 0;
    let high = arr.length - 1;
    while (low <= high) {
        let mid = Math.floor( (low + high) / 2);
        if (arr[mid] == key) {
            return mid;
        } else if (arr[mid] < key) {
            low = mid + 1; //偏大，调整low 这个 + 1
        } else {
            high = mid - 1; //偏小，调整high 这个 - 1
        }
    }
    return -1;
}

const arr = [1, 4, 5, 6, 7, 8, 10, 11, 23, 42, 44, 54, 56, 77, 102]
console.log(bsearch(arr, 44))

/**
 * 二分查找
 * @param arr
 * @param low
 * @param high
 * @param target
 * @returns {number|number|number|*}
 */
function bsearchInternally(arr, low, high, target) {
    if (low > high) return -1;
    let mid = Math.floor((low + high) / 2);
    if (arr[mid] == target) {
        return mid;
    } else if (arr[mid] < target) {
        return bsearchInternally(arr, mid + 1, high, target);
    } else {
        return bsearchInternally(arr, low, mid - 1, target);
    }
}

/**
 * 二分查找入口
 * @param arr
 * @param target
 * @returns {number|number|number|*}
 */
function bsearch_recuision(arr, target) {
    let low = 0;
    let high = arr.length - 1;
    return bsearchInternally(arr, low, high, target);
}
const arr2 = [1, 4, 5, 6, 7, 8, 10, 11, 23, 42, 44, 54, 56, 77, 102]
console.log(bsearch_recuision(arr2, 102))

/**
 * 二分法求平方根
 * @param n
 */
function sqrt(x) {
    const error = 0.000001;
    let lowerLimit = 0;
    let upperLimit = x;
    while (error < upperLimit - lowerLimit) {
        let mid = (upperLimit + lowerLimit) / 2
        if (mid * mid == x) {
            return mid;
        } else if (mid * mid > x) {
            upperLimit = mid
        } else {
            lowerLimit = mid;
        }
    }
    return (upperLimit + lowerLimit) / 2;
}


console.log(sqrt(8))

/**
 * 牛顿迭代法求平方根
 * 下面这种方法可以很有效地求出根号a的近似值：首先随便猜一个近似值x，然后不断令x等于x和a/x的平均数，迭代个六七次后x的值就已经相当精确了。
 * 例如，我想求根号2等于多少。假如我猜测的结果为4，虽然错的离谱，但你可以看到使用牛顿迭代法后这个值很快就趋近于根号2了：
 * (       4  + 2/   4     ) / 2 = 2.25
 * (    2.25  + 2/   2.25  ) / 2 = 1.56944..
 * ( 1.56944..+ 2/1.56944..) / 2 = 1.42189..
 * ( 1.42189..+ 2/1.42189..) / 2 = 1.41423..
 * @param n
 * @returns {number}
 */
function sqrtNewton(x) {
    if (x == 0) return 0;
    const error = 0.0001; //误差
    let guess  = (x * x + x / x) / 2;
    while ((guess * guess - x) > error) {
        guess = (guess + x/guess) / 2;
    }
    return Math.floor(guess);
}

console.log(sqrtNewton(4))