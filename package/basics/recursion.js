/**
 * simplest recuision  1 + 1 + 1 + 1 + ... 写出递推公式，找到终止条件，代数前几位理解
 * 写递归代码的关键就是找到如何将大问题分解为小问题的规律，并且基于此写出递推公式，然后再推敲终止条件，最后将递推公式和终止条件翻译成代码
 * 递归代码要警惕堆栈溢出,边界条件没有考虑，比如 x<=0
 * 递归代码要警惕重复计算
 * 调试递归:
 * 1.打印日志发现，递归值。
 * 2.结合条件断点进行调试。
 * @param n
 * @returns {number|*}
 */
function f(n) {
    if (n === 1) return 1;
    return f(n - 1) + 1;
}
console.log(f(10));

/**
 *
 * @param n
 * @returns {number|*}
 */
function fib(n) {
    if (n < 2) return n;
    return fib(n - 1) + fib(n - 2)
}

module.exports = {};