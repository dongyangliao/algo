module.exports = {}
/**
 * 引用：https://zhuanlan.zhihu.com/p/262496332
 哈希表实现
 散列表（Hash table，也叫哈希表），是根据关键码值(Key value)而直接进行访问的数据结构。也就是说，它通过把关键码值映射到表中一个位置来访问记录，以加快查找的速度。这个映射函数叫做散列函数，存放记录的数组叫做散列表。

 看一个实际需求，google公司的一个上机题:
 有一个公司,当有新的员工来报道时,要求将该员工的信息加入(id,性别,年龄,住址..),当输入该员工的id时,要求查找到该员工的 所有信息.

 要求:
 1.不使用数据库,尽量节省内存,速度越快越好=>哈希表(散列)
 2.使用链表来实现哈希表, 该链表不带表头 [即: 链表的第一个结点就存放雇员信息]
   添加时，保证按照id从低到高插入  [课后思考：如果id不是从 低到高插入，但要求各条链表仍是从低到高，怎么解决?]
 *
 */

//表示一个雇员
class Emp {
    constructor(id, name) {
        this.id = id;
        this.name = name;
        this.next = null;
    }
}

class EmpLinkedList {
    constructor() {
        this.head = null;
    }
    //添加雇员到链表
    //说明
    //1.假定，当添加雇员时，id是自增长，即id的分配总是从小到大
    //因此我们将雇员直接添加到本链表的最后即可
    add(emp) {
        //如果是添加第一个雇员
        if (this.head == null) {
            this.head = emp;
            return;
        }
        //如果不是第一个雇员，则使用一个辅助的指针，帮助定位到最后
        let curEmp = this.head;
        while (true) {
            if (curEmp.next == null) {
                //说明到链表最后
                break;
            }
            curEmp = curEmp.next;
        }
        //退出时直接将emp加入链表
        curEmp.next = emp;
    }
    //遍历链表的雇员信息
    list() {
        let list = [];
        if (this.head == null) {
            return list;
        }
        let curEmp = this.head;
        while (true) {
            list.push(curEmp);
            if (curEmp.next == null) {
                break
            }
            curEmp = curEmp.next
        }
        return list;
    }
    //根据id查找雇员
    //如果查找到，就返回Emp,如果没有找到，就返回null
    findEmpById(id) {
        //判断链表是否为空
        if (this.head == null) {
            return null;
        }
        let curEmp = this.head;
        while (true) {
            if (curEmp.id === id) {
                //找到
                break;
            }
            if (curEmp.next == null) {
                //没找到
                curEmp = null;
                break
            }
            curEmp = curEmp.next
        }
        return curEmp
    }
}


class HashTable {
    constructor(size) {
        this.size = size;
        this.empLinkedListArr = new Array(size);
        //分别初始化每个链表
        for (let i = 0; i < this.empLinkedListArr.length; i++) {
            this.empLinkedListArr[i] = new EmpLinkedList()
        }
    }
    // 添加雇员
    add(emp) {
        //根据员工的id，得到该员工应该添加到哪条链表
        let empLinkedListNo = this.hashFunc(emp.id);
        this.empLinkedListArr[empLinkedListNo].add(emp)
    }
    // 遍历所有的链表
    list() {
        let list = [];
        for (let i = 0; i < this.empLinkedListArr.length; i++) {
            list.push(this.empLinkedListArr[i].list())
        }
        return list
    }
    //编写散列函数，使用一个简单取模法
    /**
     * 散列表的核心是散列函数
     * @param id
     * @returns {number}
     */
    hashFunc(id) {
        return id % this.size;
    }
    //查找雇员
    findEmpById(id) {
        let empLinkedListNo = this.hashFunc(id);
        let emp = this.empLinkedListArr[empLinkedListNo]
            .findEmpById(id);
        return emp;
    }
}

let HT = new HashTable(7);
let emp = new Emp(1, 'zzz');
let emp2 = new Emp(3, 'zzz3');
let emp8 = new Emp(8, 'zzz8');
HT.add(emp);
HT.add(emp2);
HT.add(emp8);
console.log('散列表：', HT.list());
console.log('查找id为3的雇员：', HT.findEmpById(3));
console.log('查找id为8的雇员：', HT.findEmpById(8));
console.log('查找id为2的雇员：', HT.findEmpById(2));

/**
 * hash算法
 * 有：MD5、SHA等，推荐SHA(Secure Hash Algorithm)
 * 特点
 * 1）唯一标识，识别持续用户、负载均衡等
 * 2）分布式存储
 * 3）安全加密
 * 4）数据校验MD5 检测会不会背篡改
 * 5）数据分片
 */