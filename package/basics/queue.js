class Queue {
    constructor(n) {
        this.items = new Array(n);
        this.n = n; // total
        this.head = 0; //头出
        this.tail = 0; //尾入
    }
    enqueue(item) {
        if(this.tail === this.n) {
            if (this.head === 0) return false; // tail = 10 head 0 全满
            // data transfer 比如 head 4 i 5  -> index 5-4 = 1
            for(let i = this.head; i < this.tail; i++) {
                this.items[i - this.head] = this.items[i];
            }
            this.tail -= this.head;
            this.head = 0;
        }
        this.items[this.tail++] = item;
        return true;
    }
    dequeue() {
        if (this.head === this.tail) return false;
        const ret = this.items[this.head];
        this.head++;
        return ret;
    }
    display() {
        for(let i = this.head; i < this.tail; i++) {
            process.stdout.write(this.items[i] + ' ');
        }
    }
}

const queue = new Queue(5);
console.log(queue.enqueue(1));
console.log(queue.enqueue(1));
console.log(queue.enqueue(1));
console.log(queue.enqueue(1));
console.log(queue.enqueue(1));
console.log(queue.enqueue(1));
console.log(queue.dequeue(1));
console.log(queue.dequeue(1));
queue.display();


/**
 * 另一类简单（动态）
 */
let aQueue = [1, 2, 3, 4, 5];
// 队尾in
aQueue.push(6);    // 存入 arr -> [1, 2, 3, 4, 5, 6]
// 队头out
aQueue.shift();    // 取出 arr -> [2, 3, 4, 5, 6]

module.exports = {};