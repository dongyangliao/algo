function Node(element) {
    this.element = element;
    this.next = null;
}

function linkedList() {
    this.head = new Node('head');
    this.find = find;
    this.insert = insert;
    this.remove = remove;
    this.display = display;
    this.findPrevious = findPrevious;
    this.reverse = reverse;
}

function find(target) {
    let cursorNode = this.head;
    while (cursorNode.element !== target) {
        cursorNode = cursorNode.next;
    }
    return cursorNode;
}

function findPrevious(target) {
    let prevNode = this.head;
    while (prevNode.next !== null && prevNode.next.element !== target ) {
        prevNode = prevNode.next;
    }
    return prevNode;
}

function insert(newElement, indexElement) {
    const insertNode = new Node(newElement);
    const indexNode = this.find(indexElement);
    insertNode.next = indexNode.next; //附新
    indexNode.next = insertNode; // 自附
}

function remove(removeElement) {
    const prevNode = this.findPrevious(removeElement);;
    if (prevNode.next !== null) {
        prevNode.next = prevNode.next.next;
    }
}

function reverse() {
    //
}

function display() {
    let node = this.head;
    while (node.next !== null) {
        console.log(node.next.element);
        node = node.next;
    }
}

const LINK = new linkedList();
LINK.insert('conway', 'head');
LINK.insert('wwww', 'conway');
LINK.insert('qwer', 'conway');
LINK.display();
LINK.remove('wwww');
LINK.display();

module.exports = {};