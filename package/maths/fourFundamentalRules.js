module.exports = {}
/**
 * 实现一个四则运算语法解析器
 * https://zhuanlan.zhihu.com/p/112460676
 * 可以将token归纳为下面几类：
 *  1)字母 （ALPHABET）
 *  2)左括号（LEFT_PARENTHESES）
 *  3)右括号（RIGHT_PARENTHESES）
 *  4)运算符：加（PLUS），减（SUBTRACT），乘（MULTIPLE），除（DIVISION）
 */

/**
 * TOKEN正则表达式
 * @type {{ALPHABET: {pattern: RegExp, type: string}}}
 */
const TOKEN_MAPS = {
    ALPHABET: {
        type: 'ALPHABET',
        pattern: /[a-zA-Z]/,
    }
    // ...
};

const tokens = [
    { type: 'LEFT_PARENTHESES', lexeme: '(' },
    { type: 'ALPHABET', lexeme: 'A' },
    { type: 'SUBTRACT', lexeme: '-' },
    { type: 'ALPHABET', lexeme: 'B' },
    { type: 'RIGHT_PARENTHESES', lexeme: ')' },
    { type: 'MULTIPLE', lexeme: '*' },
    { type: 'ALPHABET', lexeme: 'C' },
];


/**
 * 以下都是这个网站
 * https://www.cnblogs.com/amiezhang/p/11070618.html
 * @param source
 * @returns {*}
 */
function expression(source) {
    // source[0]位置尽可能多地聚合出一个AddExpression
    addExpression(source)
    // 符合Expression语法定义
    if (source[0].type === 'AddExpression' && source[1] && source[1].type === 'EOF') {
        let node = {
            type: "Expression",
            children: [source.shift(), source.shift()]
        }
        source.unshift(node)
    }
    // 出口，如果第0项不是Expression，则是抽象语法树解析错误
    if (source[0].type === 'Expression') {
        return source[0]
    } else {
        throw new Error('解析AST不正确')
    }
}

function addExpression(source) {
    // MultipleExpression --> AddExpression
    if (source[0].type === 'MultipleExpression') {
        let node = {
            type: "AddExpression",
            children: [source[0]]
        }
        source.shift();
        source.unshift(node)
        // 继续递归，寻求合并
        return addExpression(source);
    }
    // AddExpression聚合
    if (source[0].type === 'AddExpression' && source[1] && (source[1].type === '+' || source[1].type === '-')) {
        let node = {
            type: "AddExpression",
            operator: source[1].type,
            children: [source.shift(), source.shift()]
        }
        multipleExpression(source)
        node.children.push(source.shift())
        source.unshift(node)
        // 继续递归，寻求合并
        return addExpression(source);
    }
    // source[0]位置尽可能多地聚合出一个MultipleExpression
    multipleExpression(source)
    // 递归出口，如果第0项不是AddExpression，则一直递归自己
    if (source[0].type === 'AddExpression') {
        return source
    }
    // 递归自己
    return addExpression(source)
}

function multipleExpression(source) {
    // number --> MultipleExpression
    if (source[0].type === 'number') {
        let node = {
            type: "MultipleExpression",
            children: [source[0]]
        }
        source.shift();
        source.unshift(node)
        // 继续递归，寻求合并
        return multipleExpression(source);
    }
    // MultipleExpression聚合
    if (source[0].type === 'MultipleExpression' && source[1] && (source[1].type === '*' || source[1].type === '/')) {
        let node = {
            type: "MultipleExpression",
            operator: source[1].type,
            children: [source.shift(), source.shift()]
        }
        node.children.push(source.shift())
        source.unshift(node)
        // 继续递归，寻求合并
        return multipleExpression(source);
    }
    // 递归出口，如果第0项不是MultipleExpression，则一直递归自己
    if (source[0].type === 'MultipleExpression') {
        return source
    }
}

function evaluate(node) {
    if (node.type === 'Expression') {
        return evaluate(node.children[0])
    }
    if (node.type === 'AddExpression') {
        if (node.operator === '+') {
            return evaluate(node.children[0]) + evaluate(node.children[2])
        }
        if (node.operator === '-') {
            return evaluate(node.children[0]) - evaluate(node.children[2])
        }
        return evaluate(node.children[0])
    }
    if (node.type === 'MultipleExpression') {
        if (node.operator === '*') {
            return evaluate(node.children[0]) * evaluate(node.children[2])
        }
        if (node.operator === '/') {
            return evaluate(node.children[0]) / evaluate(node.children[2])
        }
        return evaluate(node.children[0])
    }
    if (node.type === 'number') {
        return Number(node.token)
    }
}

// 上节词法分析获取的词
let tokenList =
    [
        {"type": "number","token": "123"},
        {"type": "*","token": "*"},
        {"type": "number","token": "656"},
        {"type": "-","token": "-"},
        {"type": "number","token": "644"},
        {"type": "+","token": "+"},
        {"type": "number","token": "3131"},
        {"type": "EOF","token": "EOF"}
    ]
const ast = expression(tokenList)
// 打印出解析出来的ast
console.log(JSON.stringify(ast,null,"\t"));

console.log(evaluate(ast));