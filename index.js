/**
 * linkednode
 */
// require("./package/basics/linkednode.js");

/**
 * stack
 */
// require('./package/basics/stack.js');

/**
 * queue
 */
// require('./package/basics/queue.js');

/**
 * recursion
 */
// require('./package/basics/recursion.js');

/**
 * sort
 */
// require('./package/basics/sort.js');

/**
 * search (binary search)
 */
// require('./package/basics/search.js');

/**
 * skiplist
 */
// require('./package/basics/skiplist.js');

/**
 * hash
 */
// require('./package/basics/hash.js');

/**
 * hash
 */
require('./package/maths/fourFundamentalRules.js');

/**
 * express
 */
console.log('\nstart express server...')
const fs = require('fs');
const path = require('path');
const express = require('express');
const app = express();
app.use(express.static(path.resolve(__dirname, './web')))
app.get('*', function(req, res) {
    const html = fs.readFileSync(path.resolve(__dirname, './web/index.html'), 'utf-8')
    res.send(html)
})
app.listen(8082);